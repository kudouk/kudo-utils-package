module bitbucket.org/kudouk/kudo-utils-package

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/shopspring/decimal v1.2.0
	go.mongodb.org/mongo-driver v1.5.0
)
