package utils

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strings"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"go.mongodb.org/mongo-driver/bson"
)

type Money struct {
	amount *decimal.Decimal
}

// func (m *Money) UnmarshalJSON(data []byte) error {
// 	fmt.Println("money", m, data)
// 	return nil
// }

// func (m *Money) MarshalJSON() ([]byte, error) {
// 	fmt.Println("money", m)
// 	return []byte{}, nil
// }

func NewMoneyFromString(amount string) (Money, error) {
	d, err := decimal.NewFromString(amount)
	if err != nil {
		return Money{}, errors.Wrap(err, "Money.NewMoneyFromString: invalid string format")
	}
	return Money{
		amount: &d,
	}, nil
}

func NewMoneyFromInt(amount int) Money {
	d := decimal.NewFromInt(int64(amount))
	dec := d.Div(decimal.NewFromInt(100))

	return Money{
		amount: &dec,
	}
}

func NewMoneyFromInt64(amount int64) Money {
	d := decimal.NewFromInt(amount)
	dec := d.Div(decimal.NewFromInt(100))

	return Money{
		amount: &dec,
	}
}

func NewMoneyFromFloat32(amount float32) Money {
	dec := decimal.NewFromFloat32(amount)

	return Money{
		amount: &dec,
	}
}

func (m Money) GetString() string {
	if m.amount == nil {
		return "0"
	}
	return m.amount.String()
}

func (m Money) GetInt64() int64 {
	if m.amount == nil {
		return 0
	}
	return m.amount.Mul(decimal.NewFromInt(100)).IntPart()
}

func (m Money) AddTax(a string) (Money, error) {
	if m.amount == nil {
		return Money{}, nil
	}
	if a == "" {
		a = "0"
	}
	tax, err := decimal.NewFromString(a)
	if err != nil {
		return Money{}, errors.Wrap(err, "Money.AddTax: invalid string format")
	}

	withTax := m.amount.Mul(tax.Add(decimal.NewFromFloat(1))).Round(2)

	return Money{
		amount: &withTax,
	}, nil
}

func (m Money) Add(a Money) Money {
	if m.amount == nil {
		m = NewMoneyFromInt(0)
	}
	if a.amount == nil {
		zero, err := decimal.NewFromString("0")
		if err != nil {
			return Money{}
		}
		a.amount = &zero
	}
	d2 := m.amount.Add(*a.amount)

	return Money{
		amount: &d2,
	}
}

func (m Money) Sub(a Money) Money {
	if m.amount == nil {
		m = NewMoneyFromInt(0)
	}
	if a.amount == nil {
		zero, err := decimal.NewFromString("0")
		if err != nil {
			return Money{}
		}
		a.amount = &zero
	}
	d2 := m.amount.Sub(*a.amount)

	return Money{
		amount: &d2,
	}
}

func (m Money) Div(a Money) Money {
	if m.amount == nil {
		return Money{}
	}
	if a.amount == nil {
		zero, err := decimal.NewFromString("0")
		if err != nil {
			return Money{}
		}
		a.amount = &zero
	}
	d2 := m.amount.Div(*a.amount).Round(2)

	return Money{
		amount: &d2,
	}
}

func (m Money) Mul(a Money) Money {
	if m.amount == nil {
		return Money{}
	}
	if a.amount == nil {
		zero, err := decimal.NewFromString("0")
		if err != nil {
			return Money{}
		}
		a.amount = &zero
	}
	d2 := m.amount.Mul(*a.amount).Round(2)

	return Money{
		amount: &d2,
	}
}

func (m Money) MulInt(a int) Money {
	if m.amount == nil {
		return Money{}
	}

	d1 := decimal.NewFromInt(int64(a))
	d2 := m.amount.Mul(d1)

	return Money{
		amount: &d2,
	}
}

func (m Money) MulFloat(a float32) Money {
	if m.amount == nil {
		return Money{}
	}

	d1 := decimal.NewFromFloat32(a)
	d2 := m.amount.Mul(d1)

	return Money{
		amount: &d2,
	}
}

func (m Money) IsZero() bool {
	if m.amount == nil {
		return true
	}
	return m.amount.IsZero()
}

func (m Money) IsSameValue(a Money) bool {
	return m.amount.Equal(*a.amount)
}

func (m Money) IsGreaterThan(a Money) bool {
	return m.amount.GreaterThan(*a.amount)
}

func (m Money) IsLessThan(a Money) bool {
	return m.amount.LessThan(*a.amount)
}

func MoneyCalcMargin(sales Money, cost Money) string {
	if sales.IsZero() {
		return "0"
	}
	c := sales.Sub(cost)
	return c.Div(sales).GetString()
}

// save to DB as int
func (m *Money) Value() (driver.Value, error) {
	if m == nil || m.amount == nil {
		return nil, nil
	}
	d1 := m.amount.Mul(decimal.NewFromInt(100))
	return d1.IntPart(), nil
}

// read from DB
func (m *Money) Scan(value interface{}) error {
	if value == nil {
		m.amount = nil
		return nil
	}

	ns := sql.NullInt64{}
	if err := ns.Scan(value); err != nil {
		return errors.Wrap(err, "Money.Scan: error converting value from DB")
	}

	if !ns.Valid {
		return errors.New("Money.Scan: column is not nullable")
	}

	d1 := decimal.NewFromInt(ns.Int64)
	dec := d1.Div(decimal.NewFromInt(100))

	m.amount = &dec
	return nil
}

type AmountBSON struct {
	Amount string
}

func (m *Money) MarshalBSON() ([]byte, error) {
	t := AmountBSON{
		Amount: "0",
	}

	if m != nil {
		t.Amount = m.GetString()
	}

	return bson.Marshal(t)
}

func (m *Money) UnmarshalBSON(data []byte) error {
	v := struct {
		Amount string
	}{}
	if err := bson.Unmarshal(data, &v); err != nil {
		return err
	}

	d, err := decimal.NewFromString(v.Amount)
	if err != nil {
		return err
	}

	m.amount = &d

	return nil
}

func (m *Money) MarshalJSON() ([]byte, error) {
	// t := struct {
	// 	Amount string
	// }{
	// 	Amount: m.GetString(),
	// }
	return json.Marshal(m.GetString())
}

func (m *Money) UnmarshalJSON(data []byte) error {
	amount := strings.ReplaceAll(string(data), "\"", "")

	d, err := decimal.NewFromString(amount)
	if err != nil {
		return err
	}

	m.amount = &d

	return nil
}
